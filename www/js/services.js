'use strict';

angular.module('cleansnap.services', ['firebase', 'ngStorage'])

  .factory('Defaults', Defaults)
  .factory('Location', Location)
  .factory('Jobs', Jobs)
  .factory('Login', Login)
  .factory('Focus', Focus)
  .factory('Profiles', Profiles);



Defaults.$inject = [];
function Defaults() {
  return {
    centerUsa: {lat: 39.83, lng: -98.58},
    defaultSearchRadius: 200,
    fullUsaSearchRadius: 2000,
    defaultLocation: {lat: 40, lng: -83},
    maxSearchRadius: 500,
    minLatitude: 24.5,
    minSearchRadius: 10,
    defaultAccountLevel: 20
  }
}

Location.$inject = ['$q', 'Defaults'];
function Location($q, Defaults) {

  var svc = {
    location: {lat: null, lng: null},
    findLocation: function () {
      return $q(function (resolve, reject) {
        var options = {maximumAge: 20000, timeout: 10000, enableHighAccuracy: true};
        console.log('cccc');
          navigator.geolocation.getCurrentPosition(
          function (position) {
            console.log('eeeee');
            svc.location = {lat: position.coords.latitude, lng: position.coords.longitude}; // tied location
            /*if (position.coords.latitude > Defaults.minLatitude) {
              console.log("XXXXXX");
              svc.location = {lat: position.coords.latitude, lng: position.coords.longitude}; // tied location
            } else {
              console.log('Latitude is south of Key West, setting location to Columbus OH');
              //svc.location = {lat: 40, lng: -83};
              svc.location = Defaults.defaultLocation;
            }*/
            resolve(svc.location);
          }, function (err) {
            console.log('error getting location:', err);
            svc.location = null;
            reject(svc.location);
          }, options);
      });
    }
  };

  svc.getDirectionsUrl = function(lat, lng) {
    if (svc.location.lat && svc.location.lng) {
      var url = "https://maps.google.com/";

      if (ionic.Platform.isIOS()) {
        url = "https://maps.apple.com/";
      }

      url += "?saddr=" + svc.location.lat + "," + svc.location.lng;
      url += "&daddr=" + lat + "," + lng;

      return url;
    }
  };

  return svc;
}

Login.$inject = ['$ionicModal', '$window', '$timeout', 'rootRef',
  '$firebaseAuth', '$firebaseObject', '$localStorage', 'Defaults'];
function Login($ionicModal, $window, $timeout, rootRef,
               $firebaseAuth, $firebaseObject, $localStorage, Defaults) {
  var svc = {};

  svc.profile = {
    accountLevel: 0
  };

  svc.loginCallback = function () {
    return;
  };

  svc.showModal = function (callback) {

    $ionicModal.fromTemplateUrl('templates/signin.html', {
      scope: null,
      controller: 'LoginCtrl'
    }).then(function (modal) {
      svc.modal = modal;
      svc.loginCallback = callback;
      svc.modal.show();
    });
  };

  svc.hideModal = function () {
    svc.modal.hide();
  };

  svc.authMessage = "";

  svc.loginWithEmail = function (email, pass) {

    var authObject = $firebaseAuth(rootRef);

    authObject.$authWithPassword({email: email, password: pass})
      .then(function (authData) {
        console.log('firebase auth', authData);
        svc.authData = authData;
        svc.loginEmail = email;

        $localStorage.firebaseToken = authData.token;
        $localStorage.loginEmail = email;

        svc.loadProfile();
        svc.hideModal();

        if (authData.password.isTemporaryPassword) {
          console.log('logged in with temp password, redirect to settings');
          svc.temporaryPassword = pass;
          $window.location.href = "/#/tab/settings";
        } else {
          svc.loginCallback();
        }
      })
      .catch(function (error) {
        svc.authMessage = error.message;

        console.log("Authentication failed: ", error);
        svc.logout();
      });
  };
  svc.loadProfile = function() {
    svc.profile = $firebaseObject(rootRef.child('profiles').child(svc.authData.uid));
    svc.profile.$loaded()
      .then(function() {
        if (!svc.profile.accountLevel) {
          svc.profile.accountLevel = Defaults.defaultAccountLevel;
        }
        if (!svc.profile.searchRadius) {
          svc.profile.searchRadius = Defaults.defaultSearchRadius;
        }
        svc.profile.$save();
      });
  };

  svc.logout = function () {
    svc.profile.accountLevel = 0;
    svc.authData = null;
    svc.loginEmail = null;

    delete $localStorage.firebaseToken;
    delete $localStorage.loginEmail;
  };


  svc.forgotPassword = function (email) {

    //console.log('loginEmail', email, pass);

    var authObject = $firebaseAuth(rootRef);

    authObject.$resetPassword({email: email})
      .then(function () {
        console.log("Password reset email sent");
        svc.authMessage = "Password reset email sent";
      })
      .catch(function (error) {
        console.error("Error: ", error);
        svc.authMessage = error.message;
      });

  };

  svc.changePassword = function(credentials) {
    var authObject = $firebaseAuth(rootRef);

    console.log(credentials);
    authObject.$changePassword(credentials)
      .then(function () {
        console.log("Password changed successfully!");
        svc.authMessage = "Password changed successfully!";
        $timeout(function() {svc.authMessage = ''}, 2000);
        svc.temporaryPassword = null;
      })
      .catch(function (error) {
        console.error("Error: ", error);
        svc.authMessage = error.message;
      });
  };

  // if previously logged in, then reconnect to firebase with token from local storage
  if ($localStorage.firebaseToken && svc.authData == null) {
    console.log('auto login');
    var authObject = $firebaseAuth(rootRef);
    authObject.$authWithCustomToken($localStorage.firebaseToken)
      .then(function (authData) {
        console.log('firebase auth', authData);
        svc.authData = authData;

        svc.loginEmail = $localStorage.loginEmail;
        svc.loadProfile();

      })
      .catch(function (error) {
        svc.authMessage = error;
        console.log("Authentication failed: ", error);
      });
  }

  return svc;
}

Jobs.$inject = ['rootRef', 'Defaults', '$firebaseObject', '$q', 'Location'];
function Jobs(rootRef, Defaults, $firebaseObject, $q, Location) {

  var svc = {};

  svc.jobsList = [];
  svc.promises = [];
  svc.locations = [];

  var fbJobs = rootRef.child('jobs');
  var fbGeos = rootRef.child('geos');
  var geoFire = new GeoFire(fbGeos);


    //svc.searchJobs = function (latLng, searchRadius, queryDoneCallback) {
  svc.searchJobs = function (latLng, searchRadius, queryDoneCallback) {
    //console.log(Settings);
    svc.promises = [];
    svc.jobsList = [];
    svc.locations = [];

    console.log(latLng);
    // Create a GeoQuery centered at specified lat/long with radius in km
    var geoQuery = geoFire.query({
      center: [latLng.lat, latLng.lng],
      radius: Math.round(searchRadius * 1.609344)
    });

    var geoQueryOnKeyEnteredn = geoQuery.on("key_entered", function (key, location) {
      // query job data for location from firebase
      svc.locations.push({lat: location[0], lon: location[1]});

      if (searchRadius <= Defaults.maxSearchRadius) {
        var qry = $firebaseObject(fbJobs.child(key));
        svc.promises.push(qry.$loaded());
      }
    });

    var geoQueryOnReady = geoQuery.on("ready", function () {
      return $q(function (resolve, reject) {

        if (searchRadius <= Defaults.maxSearchRadius) {
          $q.all(svc.promises)
            .then(function (queryData) {
              svc.jobsList = queryData;
              return queryDoneCallback();
            });
        }
        else {
          return queryDoneCallback();
        }
      });
    });

  };

  svc.getDirectionsUrl = function(job) {
    if (job.lat && job.lon && Location.location.lat && Location.location.lon) {
      var url = "https://maps.google.com/";

      if (ionic.Platform.isIOS()) {
        url = "https://maps.apple.com/";
      }

      url += "?saddr=" + Location.location.lat + "," + Location.location.lng;
      url += "&daddr=" + job.lat + "," + job.lon;

      return url;
    }
  };

  return svc;
}

Focus.$inject = ['$timeout', '$window'];
function Focus($timeout, $window) {
  return function(id) {
    // timeout makes sure that it is invoked after any other event has been triggered.
    // e.g. click events that need to run before the focus or
    // inputs elements that are in a disabled state but are enabled when those events
    // are triggered.
    $timeout(function() {
      var element = $window.document.getElementById(id);
      if(element)
        element.focus();
    });
  };
}

Profiles.$inject = ['rootRef', '$firebaseObject', '$firebaseArray', '$firebaseAuth'];
function Profiles(rootRef, $firebaseObject, $firebaseArray, $firebaseAuth) {
  var svc = {};

  function randomPassword() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < 10; i++ )
      text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
  }

  svc.data = $firebaseArray(rootRef.child('profiles').orderByChild('email'));

  svc.addUser = function(profile) {
    var authObject = $firebaseAuth(rootRef);

    authObject.$createUser({
      email: profile.email,
      password: randomPassword()
    })
      .then(function (user) {
        console.log(user);
        rootRef.child('profiles').child(user.uid).set(profile);
        authObject.$resetPassword({email: profile.email});

        // force reload
        //svc.data = null;
        //svc.data = $firebaseArray(rootRef.child('profiles').orderByChild('email'));
      });
  };

  svc.resetPassword = function (email) {
    var authObject = $firebaseAuth(rootRef);

    authObject.$resetPassword({email: email});
  };

  return svc;
}
