'use strict';

angular.module('cleansnap.controllers', ['firebase'])
  .controller('MapCtrl', MapCtrl)
  .controller('DriverCtrl', DriverCtrl)
  .controller('PremiumCtrl', PremiumCtrl)
  .controller('LoginCtrl', LoginCtrl)
  .controller('TabsCtrl', TabsCtrl)
  .controller('SettingsCtrl', SettingsCtrl)
  .controller('profilesCtrl', profilesCtrl);

/////////////////

MapCtrl.$inject = ['$scope', 'Login', 'Jobs', 'Location', 'Defaults', '$timeout'];
function MapCtrl($scope, Login, Jobs, Location, Defaults, NgMap, $timeout) {

  //Defaults.$loaded().then(function() {
    console.log(Defaults);
    $scope.markers = [];
    $scope.searchPlace = " ";
    var init = function(){
      NgMap.getMap().then(function (map) {
        console.log(map.getCenter());
        console.log('markers', map.markers);
        console.log('shapes', map.shapes);
      });
    };


    function removeMarkers() {
      _.forEach($scope.markers, function (marker) {
        marker.setMap(null);
        marker = null;
      });
      $scope.markers = [];
    }

    function addMarker(lat, lng) {
      var directionsUrl;

      if (showDirections) {
        directionsUrl = Location.getDirectionsUrl(lat, lng);
      }

      var marker = new google.maps.Marker({
        map: mapObject,
        animation: google.maps.Animation.DROP,
        position: {lat: lat, lng: lng},
        url: directionsUrl
      });

      if (directionsUrl) {
        //google.maps.event.addListener(marker, 'click', function () {
        //  window.location.href = this.url;  //changed from markers[i] to this
        //});
        var infowindow = new google.maps.InfoWindow({
          content: "<a href='" + directionsUrl + "' target='_blank'>Directions</a>"
        });
        marker.addListener('click', function() {
          infowindow.open(mapObject, marker);
        });
      }

      $scope.markers.push(marker);
    }

    $scope.settings = Login.profile;
    $scope.searchButtonText = "Finding your location, please wait...";


    function findLocation() {
      Location.findLocation()
        .then(function (location) {
          console.log('found location', Location);
          $scope.searchButtonText = "Show Me Jobs";
        })
        .catch(function () {
          console.log('failed to find location', Location);
          $scope.searchButtonText = "Couldn't find your location, try again?";
        });
    }
    // run as form loads
    findLocation();

    // have to use this stupid callback because we had to stack promises over a geoquery event
    // hopefully can make better with observable later
    var queryDoneCallback = function () {
      //console.log('call me back!', Jobs.jobList);
      // console.log('qcb', Jobs.locations);

      _.forEach(Jobs.locations, function (location) {
        if (location.lat) {
          //console.log(job, job.lat, job.lon);
          addMarker(location.lat, location.lon);
        } else {
          console.log('addmarker failed - invalid location', location);
        }
      });

      // add blue pin in the center of the search area
      if (Location.location.lat) {
        var marker = new google.maps.Marker({
          map: mapObject,
          animation: google.maps.Animation.DROP,
          position: Location.location,
          icon: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png',
          zIndex: google.maps.Marker.MAX_ZINDEX + 1
        });
        $scope.markers.push(marker);
      }
    };


    $scope.searchClick = function(searchPlace) {
      // if not logged in, pop up the login dialog
      console.log(Login.profile.accountLevel);
      console.log(searchPlace);
      $scope.searchPlace = searchPlace;
      if (Login.profile.accountLevel == 0) {
        Login.showModal(jobSearch);
      }
      else if ($scope.searchButtonText == "Show Me Jobs") {
        console.log('bbb');
        jobSearch();

      }
      else if ($scope.searchButtonText == "Couldn't find your location, try again?") {
        console.log('eeee');
        findLocation();
      } 
    };



    var searchArea; // local variable to hold map circle object
    var showDirections = false;

    //var map = new google.maps.Map(document.getElementById("map"),
    //  {
    //  center: "loc"
    //});




   
    var geocoder = new google.maps.Geocoder();
    console.log(geocoder);

    var jobSearch = function() {
      $scope.loc = { lat: 40, lon: -73 };

      $scope.gotoLocation = function (lat, lon) {

        if ($scope.lat != lat || $scope.lon != lon) {
          $scope.loc = { lat: lat, lon: lon };
          if (!$scope.$$phase) $scope.$apply("loc");
        }
      };

      console.log($scope.searchPlace);
      if ($scope.searchPlace) {
        geocoder.geocode({ 'address': $scope.searchPlace }, function (results, status) {
          console.log(status);
          if (status == google.maps.GeocoderStatus.OK) {
            var loc = results[0].geometry.location;
            $scope.searchPlace = results[0].formatted_address;
            Location.location.lat = loc.lat();
            Location.location.lng = loc.lng();
            $scope.gotoLocation(loc.lat(), loc.lng());
            console.log(loc);
            console.log(Location.location);
            
          } else {
            alert("Sorry, this search produced no results.");
          }
        });
      }

      // reset map zoom level if we're logged in
      if (Login.profile.accountLevel > 0) {
        mapObject.maxZoom = 22;
        showDirections = true;
      }
      else {
        mapObject.maxZoom = 7;
      }

      // clear off any existing markers
      removeMarkers();

      var searchRadius = Login.profile.searchRadius ? Login.profile.searchRadius : Defaults.defaultSearchRadius;

      var newSearchArea = new google.maps.Circle({
          strokeColor: '#0000FF',
          strokeOpacity: 0.4,
          strokeWeight: 1,
          fillColor: '#0000FF',
          fillOpacity: 0.05,
          map: mapObject,
          center: Location.location,
          radius: Math.round(searchRadius * 1609.344) // convert from miles to meters
        });
      var bounds = newSearchArea.getBounds();
      mapObject.fitBounds(bounds);

      console.log(newSearchArea);
      if (searchArea) {
        //searchArea.setMap(null);
      }
      searchArea = newSearchArea;

      Jobs.searchJobs(Location.location, searchRadius, queryDoneCallback);
  };

  var mapOptions = {
    center: Defaults.centerUsa,
    zoom: 2,
    minZoom: 4,
    maxZoom: 10,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    streetViewControl: false,
    mapTypeControl: false
  };

  var mapObject = new google.maps.Map(document.getElementById("map"), mapOptions);

    /*$timeout(function(){
      init();
    },100);*/


    google.maps.event.addListenerOnce(mapObject, 'idle', function () {
      // do something only the first time the map is loaded

      // push the "get jobs" button into the map
      //var centerControlDiv = document.getElementById("mapButton");
      //centerControlDiv.index = 1;
      //mapObject.controls[google.maps.ControlPosition.BOTTOM_CENTER].push(centerControlDiv);

      Jobs.searchJobs(Defaults.centerUsa, Defaults.fullUsaSearchRadius, queryDoneCallback);
    });
  //});

}

///////////////

DriverCtrl.$inject = ['$scope', 'Jobs', 'Location'];
function DriverCtrl($scope, Jobs, Location) {
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  $scope.getDirectionsUrl = function(lat, lng) {
    return Location.getDirectionsUrl(lat, lng);
  };

  $scope.$on('$ionicView.enter', function (e) {
    //$scope.jobs = null;
    $scope.jobs = Jobs.jobsList;
    //console.log($scope.jobs)
  });
  //$scope.remove = function (job) {
  //  Jobs.remove(job);
  //};
}

///////////////

PremiumCtrl.$inject = ['$scope', 'Jobs', 'Location'];
function PremiumCtrl($scope, Jobs, Location) {
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  $scope.getDirectionsUrl = function(lat, lng) {
    return Location.getDirectionsUrl(lat, lng);
  };

  $scope.$on('$ionicView.enter', function (e) {
    //$scope.jobs = null;
    $scope.jobs = Jobs.jobsList;
    //console.log($scope.jobs)
;  });
  //$scope.remove = function (job) {
  //  Jobs.remove(job);
  //};
}

///////////////

SettingsCtrl.$inject = ['$scope', '$location', 'Login', 'Defaults', 'Focus'];
function SettingsCtrl($scope, $location, Login, Defaults, Focus) {
  Login.profile = Login.profile;
  $scope.login = Login;
  //console.log(Settings);
  if (Login.profile.searchRadius > Defaults.maxSearchRadius) {
    $scope.searchRadius = Defaults.defaultSearchRadius;
  } else {
    $scope.searchRadius = Login.profile.searchRadius;
  }
  $scope.radiusChanged = function(searchRadius) {
    var searchRadiusInt = parseInt(searchRadius)
    if (searchRadiusInt >= Defaults.minSearchRadius && searchRadiusInt <= Defaults.maxSearchRadius) {
      Login.profile.searchRadius = searchRadiusInt;
    }
    else {
      Login.profile.searchRadius = Defaults.defaultSearchRadius;
    }
    Login.profile.$save();
  };
  $scope.logout = function() {
    Login.logout();
    $location.path('/#/tab/map');
  };

  $scope.changePasswordData = {
    oldPassword: '',
    newPassword: '',
    confirmPassword: ''
  };
  console.log($scope.changePasswordData);

  $scope.changePasswordData.email = Login.profile.loginEmail;

  $scope.changePassword = function() {
    if (!$scope.changePasswordData.oldPassword) {
      Login.authMessage = "Please enter current password";
    }
    else if (!$scope.changePasswordData.newPassword) {
      Login.authMessage = "Please enter new password";
    }
    else if (!$scope.changePasswordData.confirmPassword) {
      Login.authMessage = "Please confirm new password";
    }
    else if ($scope.changePasswordData.newPassword != $scope.changePasswordData.confirmPassword) {
      Login.authMessage = "New password values do not match";
    }
    else {
      var changePasswordData = {
        email: Login.loginEmail,
        oldPassword: $scope.changePasswordData.oldPassword,
        newPassword: $scope.changePasswordData.newPassword
      };
      Login.changePassword(changePasswordData);
    }
  };

  $scope.$on('$ionicView.enter', function (e) {
    if (Login.temporaryPassword) {
      $scope.changePassword.oldPassword = Login.temporaryPassword;
      // set focus to new password
      Focus('newPassword');
    }
  });
}
///////////////

TabsCtrl.$inject = ['$scope', 'Login'];
function TabsCtrl($scope, Login) {

  $scope.minAccountLevel = function(accountLevel) {
    if (Login.profile.accountLevel < accountLevel) {
      return "ng-hide";
    } else {
      return "ng-show";
    }
  };
}

///////////////

LoginCtrl.$inject = ['$scope', 'Login'];
function LoginCtrl($scope, Login) {

  //$scope.Login = Login;

  $scope.loginEmail = function () {
    console.log(Login);
    if ($scope.email && $scope.pass) {
      Login.loginWithEmail($scope.email, $scope.pass);
    }
  };

  $scope.forgotPassword = function () {
    Login.forgotPassword($scope.email);
  };

}

profilesCtrl.$inject = ['$scope', '$timeout', 'Focus', 'Profiles'];
function profilesCtrl($scope, $timeout, Focus, Profiles) {

  $scope.profiles = Profiles.data;
  var newProfile = null;

  $scope.dataChanged = function(key) {
    console.log(key);
    var profile = $scope.profiles.$getRecord(key);
    console.log(profile);
    profile.$dirty = true;
  };

  $scope.isDirty = function(key) {
    var profile = $scope.profiles.$getRecord(key);
    return (profile.$dirty == true);
  };


  $scope.saveUser = function(profile) {
    console.log(profile);
    var email = profile.email;
      if (profile.$id) {
        $scope.profiles.$save(profile);
      }
      else {
        if (profile.email && profile.name) {
          // check for dup email!
          Profiles.addUser({
            email: profile.email,
            name: profile.name,
            accountLevel: profile.accountLevel
          });
          console.log("nope");
          newProfile = null;
          $scope.profiles.pop();
        }
      }
  };


  $scope.addUser = function() {
    console.log('add user');
    if (!newProfile) {
      newProfile = {email: '', name: '', accountLevel: 0};
      $scope.profiles.push(newProfile);
      // push focus to new profile
      Focus('new-email');
    }
  };

  //$scope.resetPasswordText = "Reset Password";

  var resetEmail = null;

  $scope.resetPasswordText = function(email) {
    if (email === resetEmail) {
      return "Reset Password Email Sent!";
    } else {
      return "Reset Password";
    }
  };

  $scope.resetPassword = function(email) {
    Profiles.resetPassword(email);
    //$scope.resetPasswordText = "Reset Password Email Sent!";
    resetEmail = email;
    $timeout(function() {resetEmail = null;}, 3000);
    //$timeout(function() {$scope.resetPasswordText = "Reset Password";}, 3000);
  };
}
