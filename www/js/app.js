// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'cleansnap.services' is found in services.js
// 'cleansnap.controllers' is found in controllers.js
angular.module('cleansnap', ['ionic', 'cleansnap.controllers', 'cleansnap.services', 'firebase', 'angularSpinners'])
  .constant('FirebaseUrl', 'https://cleansnap-dev.firebaseio.com/')
  .constant('FirebaseKey', 'KFHckSgoQunBOnatgcJYRqbl3NBQmhlu4W0TT7Om')
  //.constant('FirebaseUrl', 'https://cleansnap.firebaseio.com/')
  //.constant('FirebaseKey', 'kHgTofdz4YIltI32VII2jNrTFprwyXl528Xc44dG')
  .service('rootRef', ['FirebaseUrl', Firebase])
  .run(ApplicationRun)
  .config(ApplicationConfig);


ApplicationRun.$inject = ['$ionicPlatform'];
function ApplicationRun($ionicPlatform) {
  $ionicPlatform.ready(function () {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
    console.log('running on ', ionic.Platform.platform());
  });
}


ApplicationConfig.$inject = ['$stateProvider', '$urlRouterProvider'];
function ApplicationConfig($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  // setup an abstract state for the tabs directive
    .state('tab', {
      url: '/tab',
      abstract: true,
      templateUrl: 'templates/tabs.html',
      controller: 'TabsCtrl'
    })

    // Each tab has its own nav history stack:

    .state('tab.map', {
      url: '/map',
      views: {
        'tab-map': {
          templateUrl: 'templates/tab-map.html',
          controller: 'MapCtrl'
        }
      }
    })

    .state('tab.driver', {
      url: '/driver',
      views: {
        'tab-driver': {
          templateUrl: 'templates/tab-driver.html',
          controller: 'DriverCtrl'
        }
      }
    })
    .state('tab.premium', {
      url: '/premium',
      views: {
        'tab-premium': {
          templateUrl: 'templates/tab-premium.html',
          controller: 'PremiumCtrl'
        }
      }
    })
    .state('tab.job-detail', {
      url: '/premium/:jobId',
      views: {
        'tab-premium': {
          templateUrl: 'templates/job-detail.html',
          controller: 'JobDetailCtrl'
        }
      }
    })

    .state('tab.settings', {
      url: '/settings',
      views: {
        'tab-settings': {
          templateUrl: 'templates/tab-settings.html',
          controller: 'SettingsCtrl'
        }
      }
    })
    .state('tab.profiles', {
      url: '/profiles',
      views: {
        'tab-profiles': {
          templateUrl: 'templates/tab-profiles.html',
          controller: 'profilesCtrl'
        }
      }
    })
    .state('login', {
      url: '/',
      templateUrl: 'templates/login.html',
      controller: 'LoginCtrl'
    })
    .state('signup', {
      url: '/signup',
      templateUrl: 'templates/signup.html',
      controller: 'LoginCtrl'
    })
    .state('signin', {
      url: '/signin',
      templateUrl: 'templates/signin.html',
      controller: 'LoginCtrl'
    });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/tab/map');

}
